// Copyright (c) 2016, XMOS Ltd, All rights reserved
#ifndef _spi_h_
#define _spi_h_
#include <xs1.h>
#include <stdint.h>
#include <stddef.h>

/** This type indicates what mode an SPI component should use */
typedef enum spi_mode_t {
  SPI_MODE_0, /**< SPI Mode 0 - Polarity = 0, Clock Edge = 1 */
  SPI_MODE_1, /**< SPI Mode 1 - Polarity = 0, Clock Edge = 0 */
  SPI_MODE_2, /**< SPI Mode 2 - Polarity = 1, Clock Edge = 0 */
  SPI_MODE_3, /**< SPI Mode 3 - Polarity = 1, Clock Edge = 1 */
} spi_mode_t;


  /** Begin a transaction.
   *
   *  This will start a transaction on the bus. During a transaction, no
   *  other client to the SPI component can send or receive data. If
   *  another client is currently using the component then this call
   *  will block until the bus is released.
   *
   *  \param device_index  the index of the slave device to interact with.
   *  \param speed_in_khz  The speed that the SPI bus should run at during
   *                       the transaction (in kHZ).
   *  \param mode          The mode of spi transfers during this transaction.
   */

  void spi_begin_transaction(unsigned device_index,
                         unsigned speed_in_khz, spi_mode_t mode);

  /** End a transaction.
   *
   *  This ends a transaction on the bus and releases the component to other
   *  clients.
   */
  void spi_end_transaction(unsigned ss_deassert_time);

  /** Transfer a byte over the spi bus.
   *
   *  This function will transmit and receive 8 bits of data over the SPI
   *  bus. The data will be transmitted least-significant bit first.
   *
   *  \param data          the data to transmit the MOSI port.
   *
   *  \returns       the data read in from the MISO port.
   */
  uint8_t spi_transfer8(uint8_t data);

  /** Transfer a 32-bit word over the spi bus.
   *
   *  This function will transmit and receive 32 bits of data over the SPI
   *  bus. The data will be transmitted least-significant bit first.
   *
   *  \param data    the data to transmit the MOSI port.
   *
   *  \returns       the data read in from the MISO port.
   */
  uint32_t spi_transfer32(uint32_t data);


/*    Each slave must be connected to using the same SPI mode.

    You can access different slave devices over the interface connection
    using the device_index parameter of the interface functions.
    The task will allocate the device indices in the order of the supplied
    array of slave select ports.

    \param clk           a clock block used by the task.
    \param sclk          the SPI clock port.
    \param mosi          the SPI MOSI (master out, slave in) port.
    \param miso          the SPI MISO (master in, slave out) port.
    \param p_ss          an 4 bit multi port connected to the slave select signals
                         of the slave.
    \param num_slaves    The number of slave devices on the bus.
    \param clk           a clock for the component to use.
*/

void spi_master_init();

#endif // _spi_h_
