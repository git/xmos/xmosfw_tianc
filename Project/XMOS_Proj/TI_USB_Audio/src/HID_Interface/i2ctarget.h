//=============================================================================
// i2ctarget.h
//
// This module contains atrustures and macros used for implementing I2C 
// target cmds. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _I2CTARGET_H
#define _I2CTARGET_H

// Command codes
#define CMD_I2C_SETSLAVEADDR	0x00
#define CMD_I2C_GETSLAVEADDR	0x01
#define CMD_I2C_WRITE			0x02
#define CMD_I2C_READ			0x03
#define CMD_I2C_WRITE_16ADDR	0x04
#define CMD_I2C_READ_16ADDR		0x05

// Command structures
typedef struct _i2ctarget_slaveaddr_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option
// Bit set for 100KHZ, else for 400KHZ 
#define I2CTARGET_OPTION_100KHZ	0x01

	unsigned char optionResult;

	// Devie address to be set to
	unsigned char slaveAddess;

}I2CTARGET_SLAVEADDR;

// I2C read/write for 8 bit address devices
typedef struct _i2ctarget_data8_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// Sub-address
	unsigned char subAddess;

	// first data
	unsigned char firstData;
} I2CTARGET_DATA8;

// I2C read/write for 16 bit address devices.
typedef struct _i2ctarget_data16_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;
	unsigned char dummy;

	// Sub-address
	unsigned char subAddessHi;
	unsigned char subAddessLo;

	// first data
	unsigned char firstData;
} I2CTARGET_DATA16;




#define I2C_STOP              0x01    // Send out stop after final read/write operation.
#define I2C_START             0x02    // Send out slave address and subaddress.
#define I2C_ADDR_TYPE_BIT     0x04    // Bit on for byte addressing, off for word addressing
#define I2C_BYTE_ADDR_TYPE    0x00    // Device access using byte address
#define I2C_WORD_ADDR_TYPE    0x04    // Device access using word address
#define I2C_400_KHZ           0x08
#define I2C_READ              0x10    // Read from I2C bus.
#define I2C_WRITE             0x20    // Write to I2C bus.

#define I2C_READ_ADDR(addr)    (addr | 0x01)    // I2C read address
#define I2C_WRITE_ADDR(addr)   (addr & 0xFE)    // I2C write address

#define I2C_ERROR             (I2CSTA & ERROR)  // Indicates if slave dev. did not respond.
#define I2C_MAX_ACK_TRIES       100
//
// I2C control/status register bits
//

#define STOP_WRITE      0x01        // Stop after write transaction
#define STOP_READ       0x02        // Stop after read transaction
#define XMIT_INT_ENABLE 0x04        // Transmit interrupt enable
#define XMIT_DATA_EMPTY 0x08        // Transmit data register empty
#define FREQ_400KHZ     0x10        // Frequency select
#define ERROR           0x20        // Error condition
#define RCV_INT_ENABLE  0x40        // Receive interrupt enable
#define RCV_DATA_FULL   0x80        // Receive data register full

#define CLEAR_ALL       0x54

#define I2C_DELAY       20000       // 20000 usec.


#endif // _I2CTARGET_H
