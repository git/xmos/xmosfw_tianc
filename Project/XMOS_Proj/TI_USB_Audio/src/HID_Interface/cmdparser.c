//================================================== 
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//================================================== 
/*================================================== 
cmds.c: routines for handling commands/responses
//================================================*/

#include "cmds.h"
#include "gpiotarget.h"
#include <stddef.h>
#include <xccompat.h>

// Target function Table

// Target function Table
extern unsigned char (*Aic3254FcTable[])(void);
extern unsigned char (*GpioFcTable[])(void);
extern unsigned char (*TimerFcTable[])(void);
extern unsigned char (*Task1020RegFcTable[])(void);
extern unsigned char (*I2cTargetFcTable[])(void);
extern unsigned char (*SpiTargetFcTable[])(void);

// For Debug: Function table
unsigned char test1();
unsigned char (*TestFcTable[])(void) =
{
    test1,
    0
};

// Target table lists
#define MAX_TARGETS	7
unsigned char (**CmdsTargetTable[])(void) = {
        TestFcTable,
        Aic3254FcTable,
        GpioFcTable,
        TimerFcTable,
        Task1020RegFcTable,
        I2cTargetFcTable,
        SpiTargetFcTable,
	0x00
};

// Current AppID
unsigned char GlobCurAppID = REQ_PUBLIC_APPID;

// Global reuest and command pointers
REQ_HEADER *ReqHeaderPtr;
CMD_HEADER *CmdHeaderPtr;
 
extern unsigned char buffer_hid[512];

/*-------------------------------------------------------------------
cmdsParser(): command parser
-------------------------------------------------------------------*/
#pragma stackfunction 2048
void cmdsParser()
//void cmdsParser(unsigned char *buff)
{	
	{	
	    unsigned char len;
		unsigned char (**fc)();
		unsigned char *bpt;
		unsigned char reqOption;
	
		// Initialize request and command pointers
		ReqHeaderPtr = (REQ_HEADER *)&buffer_hid[0];
		CmdHeaderPtr =
			(CMD_HEADER *) ((unsigned long )ReqHeaderPtr + sizeof (REQ_HEADER));		
	
		// Check if SET/GET APPID
		if ((ReqHeaderPtr->appID == REQ_PUBLIC_APPID) &&
			(ReqHeaderPtr->targetID == TARGET_PUBLIC_ID))
		{
			ReqHeaderPtr->optionResult = CMD_STATUS_PASS;
			CmdHeaderPtr->optionResult = CMD_STATUS_PASS;
			switch (CmdHeaderPtr->cmdCode)
		 	{
		 	case SET_APPID: 
				GlobCurAppID = ((CMD_APPID *)CmdHeaderPtr)->appID;
				break;
			case GET_APPID:
				((CMD_APPID *)CmdHeaderPtr)->appID = GlobCurAppID;
				break;
			default:
				ReqHeaderPtr->optionResult = CMD_STATUS_INVALID_CMD;
				CmdHeaderPtr->optionResult = CMD_STATUS_INVALID_CMD;
				break;
			}
			return;
		}	
		
		// Check if AppID matched with current AppID or invalid target ID
		if (ReqHeaderPtr->appID != GlobCurAppID)
		{	
			ReqHeaderPtr->optionResult = CMD_STATUS_DEVICE_ACQUIRED;	
			return;		
		}	
		if (ReqHeaderPtr->targetID >= MAX_TARGETS)
		{	
			ReqHeaderPtr->optionResult = CMD_STATUS_INVALID_CMD;	
			return;		
		}

		// Valid target, initialize first command header, 
		// and call related target handler
		fc = CmdsTargetTable[ReqHeaderPtr->targetID];
		len = sizeof(REQ_HEADER);
		bpt = (unsigned char *)CmdHeaderPtr;
		reqOption = ReqHeaderPtr->optionResult;
	 
		// Get out for the first failure and option not REQ_OPTION_RUN_ALL
		for (ReqHeaderPtr->optionResult = CMD_STATUS_PASS; 
			 (len < ReqHeaderPtr->reqLen) && 
			 ((ReqHeaderPtr->optionResult == CMD_STATUS_PASS) || 
			  (reqOption == REQ_OPTION_RUN_ALL));
			 bpt += CmdHeaderPtr->len, len += CmdHeaderPtr->len)
		{
			CmdHeaderPtr = (CMD_HEADER*)bpt;
			CmdHeaderPtr->optionResult = fc[CmdHeaderPtr->cmdCode]();
			ReqHeaderPtr->optionResult = CmdHeaderPtr->optionResult;
		} 
	}
}

/*-------------------------------------------------------------------
test1(): test command
-------------------------------------------------------------------*/
unsigned char test1()
{
    return CMD_STATUS_PASS;
}


