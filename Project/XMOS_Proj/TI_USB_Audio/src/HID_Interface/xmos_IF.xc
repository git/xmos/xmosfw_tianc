/*-------------------------------------------------------------------
This file contains the interface functions required for GPIO, I2C and SPI
Created by : x0211286
-------------------------------------------------------------------*/

#include <stddef.h>
#include <xs1.h>
#include "stdint.h"
#include "i2c.h"
#include "spi.h"
#include "cmd_interface.h"
#include "i2ctarget.h"
#include "xc_ptr.h"


#define GPIO_0  0x01
#define GPIO_1  0x02
#define GPIO_2  0x04
#define GPIO_3  0x08
#define GPIO_4  0x10
#define GPIO_5  0x20
#define GPIO_6  0x40
#define GPIO_7  0x80

extern tileref tile[];
extern clock p_gpioclk;

on tile[1] : port p_gpio_0 = XS1_PORT_1A;
on tile[1] : port p_gpio_1 = XS1_PORT_1B;
on tile[1] : port p_gpio_2 = XS1_PORT_1C;
on tile[1] : port p_gpio_3 = XS1_PORT_1D;
on tile[1] : port p_gpio_4 = XS1_PORT_1M;
on tile[1] : port p_gpio_5 = XS1_PORT_1N;
on tile[1] : port p_gpio_6 = XS1_PORT_1O;
on tile[1] : port p_gpio_7 = XS1_PORT_1P;

static unsigned char Bank0_DIR=0;

/*-------------------------------------------------------------------
xMOS Interface function to set a bit in GPIO
-------------------------------------------------------------------*/
void Bitset_Bank0_IF(unsigned mask)
{
    // Set to 1 if direction is set to output
    mask &= (~Bank0_DIR);
    if (GPIO_0 & mask) p_gpio_0 <: 1;
    if (GPIO_1 & mask) p_gpio_1 <: 1;
    if (GPIO_2 & mask) p_gpio_2 <: 1;
    if (GPIO_3 & mask) p_gpio_3 <: 1;
    if (GPIO_4 & mask) p_gpio_4 <: 1;
    if (GPIO_5 & mask) p_gpio_5 <: 1;
    if (GPIO_6 & mask) p_gpio_6 <: 1;
    if (GPIO_7 & mask) p_gpio_7 <: 1;
}

/*-------------------------------------------------------------------
xMOS Interface function to clear a bit in GPIO
-------------------------------------------------------------------*/
void Bitclr_Bank0_IF(unsigned mask)
{
    // Set to zero if direction is set to output
    mask &= (~Bank0_DIR);
    if (GPIO_0 & mask) p_gpio_0 <: 0;
    if (GPIO_1 & mask) p_gpio_1 <: 0;
    if (GPIO_2 & mask) p_gpio_2 <: 0;
    if (GPIO_3 & mask) p_gpio_3 <: 0;
    if (GPIO_4 & mask) p_gpio_4 <: 0;
    if (GPIO_5 & mask) p_gpio_5 <: 0;
    if (GPIO_6 & mask) p_gpio_6 <: 0;
    if (GPIO_7 & mask) p_gpio_7 <: 0;
}
/*-------------------------------------------------------------------
xMOS Interface function to write a value to the GPIO port
-------------------------------------------------------------------*/
void write_Bank0_IF(unsigned value)
{
    // Write Only if direction is set to output

    unsigned char dir_mask = ~Bank0_DIR;
    if (GPIO_0 & dir_mask) { if (GPIO_0 & value) p_gpio_0 <: 1; else p_gpio_0 <: 0; }
    if (GPIO_1 & dir_mask) { if (GPIO_1 & value) p_gpio_1 <: 1; else p_gpio_1 <: 0; }
    if (GPIO_2 & dir_mask) { if (GPIO_2 & value) p_gpio_2 <: 1; else p_gpio_2 <: 0; }
    if (GPIO_3 & dir_mask) { if (GPIO_3 & value) p_gpio_3 <: 1; else p_gpio_3 <: 0; }
    if (GPIO_4 & dir_mask) { if (GPIO_4 & value) p_gpio_4 <: 1; else p_gpio_4 <: 0; }
    if (GPIO_5 & dir_mask) { if (GPIO_5 & value) p_gpio_5 <: 1; else p_gpio_5 <: 0; }
    if (GPIO_6 & dir_mask) { if (GPIO_6 & value) p_gpio_6 <: 1; else p_gpio_6 <: 0; }
    if (GPIO_7 & dir_mask) { if (GPIO_7 & value) p_gpio_7 <: 1; else p_gpio_7 <: 0; }

}
/*-------------------------------------------------------------------
xMOS Interface function to read GPIO port
-------------------------------------------------------------------*/
unsigned char Bitread_Bank0_IF()
{
    unsigned char temp=0xFF,value =0x00;
    int i=0;
    //Read only if pin is configured as input . Otherwise send value as 0
    if (Bank0_DIR & GPIO_0) p_gpio_0 :> temp; else temp=0x0;
    value |= (temp<<i++);
    if (Bank0_DIR & GPIO_1) p_gpio_1 :> temp; else temp=0x0;
    value |= (temp<<i++);
    if (Bank0_DIR & GPIO_2) p_gpio_2 :> temp; else temp=0x0;
    value |= (temp<<i++);
    if (Bank0_DIR & GPIO_3) p_gpio_3 :> temp; else temp=0x0;
    value |= temp<<i++;
    if (Bank0_DIR & GPIO_4) p_gpio_4 :> temp; else temp=0x0;
    value |= (temp<<i++);
    if (Bank0_DIR & GPIO_5) p_gpio_5 :> temp; else temp=0x0;
    value |= (temp<<i++);
    if (Bank0_DIR & GPIO_6) p_gpio_6 :> temp; else temp=0x0;
    value |= (temp<<i++);
    if (Bank0_DIR & GPIO_7) p_gpio_7 :> temp; else temp=0x0;
    value |= (temp<<i++);

    return value;
}

/*-------------------------------------------------------------------
xMOS Interface function to configure the direction of GPIO
-------------------------------------------------------------------*/

void Config_Dir_Bank0_IF(unsigned mask)
{
    Bank0_DIR= mask;
    if (GPIO_0 & mask) configure_in_port(p_gpio_0,p_gpioclk); else configure_out_port(p_gpio_0,p_gpioclk,0);
    if (GPIO_1 & mask) configure_in_port(p_gpio_1,p_gpioclk); else configure_out_port(p_gpio_1,p_gpioclk,0);
    if (GPIO_2 & mask) configure_in_port(p_gpio_2,p_gpioclk); else configure_out_port(p_gpio_2,p_gpioclk,0);
    if (GPIO_3 & mask) configure_in_port(p_gpio_3,p_gpioclk); else configure_out_port(p_gpio_3,p_gpioclk,0);
    if (GPIO_0 & mask) configure_in_port(p_gpio_4,p_gpioclk); else configure_out_port(p_gpio_4,p_gpioclk,0);
    if (GPIO_1 & mask) configure_in_port(p_gpio_5,p_gpioclk); else configure_out_port(p_gpio_5,p_gpioclk,0);
    if (GPIO_2 & mask) configure_in_port(p_gpio_6,p_gpioclk); else configure_out_port(p_gpio_6,p_gpioclk,0);
    if (GPIO_3 & mask) configure_in_port(p_gpio_7,p_gpioclk); else configure_out_port(p_gpio_7,p_gpioclk,0);

    return;
}

/*-------------------------------------------------------------------
xMOS Interface function to Access I2C read/Write. The interface
functions will be executed in tile[0]
-------------------------------------------------------------------*/
unsigned char I2C_Access_IF(client interface i_cmd cmd,unsigned char *databuf,
                    unsigned char slaveAddr,unsigned int SubAddr,
                    unsigned int nLen,unsigned char Flags)
{
    return (cmd.cmd_I2CAccess(databuf,slaveAddr,SubAddr,nLen,Flags));
}

/*-------------------------------------------------------------------
xMOS Interface function to Initialize I2C bus. The interface
functions will be executed in tile[0]
-------------------------------------------------------------------*/
void I2C_Init_IF(client interface i_cmd cmd,unsigned int bit_time)
{
    cmd.cmd_I2C_Init(bit_time);
}

/*-------------------------------------------------------------------
xMOS Interface function to start SPI transaction. The interface
functions will be executed in tile[0]
-------------------------------------------------------------------*/

void spi_begin_transaction_IF(client interface i_cmd cmd,unsigned device_index,unsigned speed_in_khz, spi_mode_t mode)
{
    cmd.cmd_spi_begin_transaction(device_index,speed_in_khz, mode);
}

/*-------------------------------------------------------------------
xMOS Interface function to stop SPI transaction. The interface
functions will be executed in tile[0]
-------------------------------------------------------------------*/
void spi_end_transaction_IF(client interface i_cmd cmd,unsigned ss_deassert_time)
{
    cmd.cmd_spi_end_transaction(ss_deassert_time);
}
/*-------------------------------------------------------------------
xMOS Interface function to send/receive 8 bit data in SPI. The interface
functions will be executed in tile[0]
-------------------------------------------------------------------*/
uint8_t spi_transfer8_IF(client interface i_cmd cmd,uint8_t data)
{
    return (cmd.cmd_spi_transfer8(data));
}

/*-------------------------------------------------------------------
xMOS Interface function for Delay
-------------------------------------------------------------------*/
void delay(unsigned char units)
{
    timer t;
    unsigned time;

    t :> time;
    t when timerafter(time + (units * 100)) :> void;
}


