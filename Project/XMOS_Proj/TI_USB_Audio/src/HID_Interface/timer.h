// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

//=============================================================================
// timer.h
//
// This module contains atrustures and macros used for implementing timer 
// target cmds. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _TIMER_H
#define _TIMER_H

// Command codes
#define CMD_TIMER_START		0x00
#define CMD_TIMER_STOP		0x01
#define CMD_TIMER_DELAY		0x02

// Command structures
typedef struct _timer_data_struct {
	// Command length	
	unsigned char len;

	// command code
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// Parameters:
    // For timerstop: return value of TF0
	// For delay, the delay value as defined
	// in firmware timer setup.
	unsigned char parm;	

} TIMER_DATA;


#endif // _GPIO_H
