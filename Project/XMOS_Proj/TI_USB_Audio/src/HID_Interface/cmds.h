//=============================================================================
// cmds.h
//
// This module contains atrustures and macros used for implementing commands. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _COMMANDS_H
#define _COMMANDS_H

#define SKIP_GPIO

// Request header
typedef struct _req_header_struct {
	// Request length that follows thisheader
	unsigned char reqLen;

	// Application ID used by host application to
	// acquire device	
	unsigned char appID;  
	
	// device/component target ID the request is directed to
	unsigned char targetID;	

	// Option for request and result for response
// Options
#define REQ_OPTION_STOP_FAIL	0x00
#define REQ_OPTION_RUN_ALL		0x01	
	// Results are the same as command results.
    unsigned char optionResult;

} REQ_HEADER;

// Command common header
typedef struct _cmd_header_struct {
	// Command length	
	unsigned char len;

	// command code
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// First parmeter of a specific targetcommand
	unsigned char parm0;

} CMD_HEADER;

// Response header
typedef struct _resp_header_struct {
	// Response length
	unsigned char len;
	
	// command code
	unsigned char cmdCode;

	// Result
	unsigned char result;

	// padding. The following used by specific target
	unsigned char padding;

} RESP_HEADER;
// Special defines for SET/GET Application ID.
#define REQ_PUBLIC_APPID 	0xFF
#define TARGET_PUBLIC_ID	0xFF
#define SET_APPID			0x00
#define GET_APPID			0x01
typedef struct _cmd_appid {
	// Command length	
	unsigned char len;

	// command code - SET_APPID or GET_APPID
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// Application ID
	unsigned char appID;
} CMD_APPID;

// Command/Response results used also for request results
#define CMD_STATUS_PASS				0x00
#define CMD_STATUS_FAIL				0x01
#define CMD_STATUS_PROCESSING		0x02
#define CMD_STATUS_TIMEOUT			0x04
#define	CMD_STATUS_INVALID_CMD		0xFE
#define	CMD_STATUS_DEVICE_ACQUIRED	0xFF

// Targets ID
#define TARGET_TEST				0
#define TARGET_AIC3254			1
#define TARGET_GPIO				2
#define TARGET_TAS1020TIMER		3
#define TARGET_TAS1020REGS		4
#define TARGET_I2C				5
#define TARGET_SPI				6

// Prototypes

extern REQ_HEADER *ReqHeaderPtr;
extern CMD_HEADER *CmdHeaderPtr;
#define RespHeaderPtr ((RESP_HEADER *)CmdHeaderPtr)

// Misc macros
#define APSET_I2C(Slave, Sub, Buffer, Len, Flag) \
		AppI2CParms.SlaveAddr = (Slave); \
		AppI2CParms.SubAddr = (Sub); \
		AppI2CParms.pBuffer_I2C = (Buffer); \
		AppI2CParms.nLen = (Len); \
		AppI2CParms.Flags = (Flag);


//#endif	// _FIRMWARE_APPLICATION

#endif // _COMMANDS_H
