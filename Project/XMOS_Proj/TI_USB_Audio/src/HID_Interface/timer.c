// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

// =============================================================================
// timer.c
// -----------------------------------------------------------------------------
// Description:
//      Not used. Available for compatibility purposes.
// =============================================================================

#include "cmds.h"
#include "timer.h"
#include "xccompat.h"

// Function prototypes
unsigned char timerStart();
unsigned char timerStop();
unsigned char timerDelay();

// Function table
unsigned char (*TimerFcTable[])(void) = {
	timerStart, timerStop,
	timerDelay,	0
}; 


extern void delay(unsigned char units);
/*-------------------------------------------------------------------
timerStart(): Start timer
-------------------------------------------------------------------*/
unsigned char timerStart()
{
/*	INIT_TIMER0; 
    SETUP_TX0(GRANULARITY);
    START_TIMER0;		*/
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
timerStop(): Bit clear
-------------------------------------------------------------------*/
unsigned char timerStop()
{
/*    STOP_TIMER0;
	((TIMER_DATA *)CmdHeaderPtr)->parm = TF0; */

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
timerDelay(): delay
-------------------------------------------------------------------*/
unsigned char timerDelay()
{
	delay(((TIMER_DATA *)CmdHeaderPtr)->parm);
	return CMD_STATUS_PASS;
}





