// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

// =============================================================================
// aic3254.c
// -----------------------------------------------------------------------------
// Description:
//      Not used. Available for compatibility purposes.
// =============================================================================

#include "cmds.h"
#include "aic3254.h"
#include "i2ctarget.h"

// Function prototypes
unsigned char aic3254Setpage();
unsigned char aic3254Getpage();
unsigned char aic3254Write8();
unsigned char aic3254Read8();
unsigned char aic3254Write16();
unsigned char aic3254Read16();
unsigned char aic3254Write32();
unsigned char aic3254Read32();

// Function table
unsigned char (*Aic3254FcTable[])(void) = {
	aic3254Setpage,	
	aic3254Getpage,
	aic3254Write8, 
	aic3254Read8,
#ifdef _FOR_FUTURE
// For future
	aic3254Write16, aic3254Read16,
	aic3254Write32, aic3254Read32,
#endif
	0
}; 

typedef struct I2C_PARAMS
{
    unsigned char SlaveAddr;
    unsigned int SubAddr;
    unsigned char* pBuffer_I2C;
    unsigned int nLen;
    unsigned char Flags;

} I2C_PARAMS;

extern I2C_PARAMS AppI2CParms;

// External reference
extern unsigned char I2CAccess();
//#define _HENRY_DEBUG

/*-------------------------------------------------------------------
aic3254Setpage(): Set page
-------------------------------------------------------------------*/
unsigned char aic3254Setpage()
{
	APSET_I2C(
		AIC3254_I2C_SLAVE_ADDRESS,0,
		&((AIC3254_PAGE_DATA *)CmdHeaderPtr)->page, 
		1,
		 I2C_WRITE);
	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
aic3254Getpage(): Get page
-------------------------------------------------------------------*/
unsigned char aic3254Getpage()
{
	APSET_I2C(
		AIC3254_I2C_SLAVE_ADDRESS,0,
		&((AIC3254_PAGE_DATA *)CmdHeaderPtr)->page, 
		1,
		I2C_READ );
	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;

}

/*-------------------------------------------------------------------
aic3254Write8(): Write bytes
-------------------------------------------------------------------*/
unsigned char aic3254Write8()
{
	APSET_I2C(
		AIC3254_I2C_SLAVE_ADDRESS,
		((AIC3254_I2C_DATA *)CmdHeaderPtr)->regAddr,
		&((AIC3254_I2C_DATA *)CmdHeaderPtr)->firstData,
	 	((AIC3254_I2C_DATA *)CmdHeaderPtr)->len - sizeof(AIC3254_I2C_DATA) + 1,
		 I2C_WRITE);
	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
aic3254Read8(): Read bytes
-------------------------------------------------------------------*/
unsigned char aic3254Read8()
{
	APSET_I2C(
		AIC3254_I2C_SLAVE_ADDRESS,
		((AIC3254_I2C_DATA *)CmdHeaderPtr)->regAddr,
		&((AIC3254_I2C_DATA *)CmdHeaderPtr)->firstData,
	 	((AIC3254_I2C_DATA *)CmdHeaderPtr)->len - sizeof(AIC3254_I2C_DATA) + 1,
		 I2C_READ );
	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}

#ifdef _FOR_FUTURE
// use these function if device support 16/32 bit access
/*-------------------------------------------------------------------
aic3254Write16(): Write 16 bit words
-------------------------------------------------------------------*/
unsigned char aic3254Write16()
{
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
aic3254Read16(): Read 16 bit words
-------------------------------------------------------------------*/
unsigned char aic3254Read16()
{
	return CMD_STATUS_PASS;
}


/*-------------------------------------------------------------------
aic3254Write32(): Write 32 bit words
-------------------------------------------------------------------*/
unsigned char aic3254Write32()
{
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
aic3254Read32(): Read 32 bit words
-------------------------------------------------------------------*/
unsigned char aic3254Read32()
{
	return CMD_STATUS_PASS;
}

#endif
