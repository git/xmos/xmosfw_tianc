// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

// =============================================================================
// spitarget.c
// -----------------------------------------------------------------------------
// Description:
//      Routines to handle SPI read/writes commands initiated by USB.
//      For future implementation.
// =============================================================================

#include "cmds.h"
#include "spitarget.h"
#include "xccompat.h"
#include "xc_ptr.h"
#include "spi.h"
#define DEVICE_INDEX    0       // Device Index of 0
#define SPI_SPEED_KHZ   1000    // 1MHZ SCLK
#define SPI_MODE        0       // Mode 0
#define SPI_DEASSERT_TIME   100

// Function prototypes
unsigned char spiTargetWrite();
unsigned char spiTargetRead();
unsigned char spiTargetWrite16();
unsigned char spiTargetRead16();

// Function table
unsigned char (*SpiTargetFcTable[])(void) = {
	spiTargetWrite, spiTargetRead, 
	spiTargetWrite16, spiTargetRead16, 
	0
}; 

extern void spi_begin_transaction_IF(CLIENT_INTERFACE(i_cmd, cmdInterface),unsigned device_index,unsigned speed_in_khz, spi_mode_t mode);
extern void spi_end_transaction_IF(CLIENT_INTERFACE(i_cmd, cmdInterface),unsigned ss_deassert_time);
extern uint8_t spi_transfer8_IF(CLIENT_INTERFACE(i_cmd, cmdInterface),uint8_t data);


/*-------------------------------------------------------------------
spiTargetWrite(): SPI write 8 bit address
-------------------------------------------------------------------*/
unsigned char spiTargetWrite()
{
	unsigned char i,j=0;
	unsigned char *bData;
	unsigned char temp_data;

    unsigned int cmdInterface;
    GET_SHARED_GLOBAL(cmdInterface,g_cmdInterface);
	//begin transaction
    spi_begin_transaction_IF(cmdInterface,DEVICE_INDEX,SPI_SPEED_KHZ,SPI_MODE);
	// Write address
    spi_transfer8_IF(cmdInterface,((SPITARGET_DATA *)CmdHeaderPtr)->address);

	//write data
	bData = &((SPITARGET_DATA *)CmdHeaderPtr)->firstData;
	for (i = sizeof(SPITARGET_DATA) - 1; 
		i < ((SPITARGET_DATA *)CmdHeaderPtr)->len; i++)
		{
        temp_data = spi_transfer8_IF(cmdInterface,bData[j]);
        bData[j++] = temp_data;
		}

	//end transaction
	spi_end_transaction_IF(cmdInterface,SPI_DEASSERT_TIME);
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
spiTargetRead(): SPI read 8 bit address
-------------------------------------------------------------------*/
unsigned char spiTargetRead()
{
    unsigned char i,j=0;
    unsigned char *bData;
    unsigned char temp_data;

    unsigned int cmdInterface;
    GET_SHARED_GLOBAL(cmdInterface,g_cmdInterface);
    //begin transaction
    spi_begin_transaction_IF(cmdInterface,DEVICE_INDEX,SPI_SPEED_KHZ,SPI_MODE);
    // Write address
    spi_transfer8_IF(cmdInterface,((SPITARGET_DATA *)CmdHeaderPtr)->address);
	bData = &((SPITARGET_DATA *)CmdHeaderPtr)->firstData;
	for (i = sizeof(SPITARGET_DATA) - 1; 
		i < ((SPITARGET_DATA *)CmdHeaderPtr)->len; i++)
		{
            temp_data = spi_transfer8_IF(cmdInterface,bData[j]);
            bData[j++] = temp_data;
		}

    //end transaction
    spi_end_transaction_IF(cmdInterface,SPI_DEASSERT_TIME);
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
spiTargetWrite16(): SPI write 16 bit address
-------------------------------------------------------------------*/
unsigned char spiTargetWrite16()
{
    unsigned char i,j=0;
    unsigned char *bData;
    unsigned char temp_data;

    unsigned int cmdInterface;
    GET_SHARED_GLOBAL(cmdInterface,g_cmdInterface);
    //begin transaction
    spi_begin_transaction_IF(cmdInterface,DEVICE_INDEX,SPI_SPEED_KHZ,SPI_MODE);
    // Write address
    spi_transfer8_IF(cmdInterface,((SPI16TARGET_DATA *)CmdHeaderPtr)->addressHi);
    spi_transfer8_IF(cmdInterface,((SPI16TARGET_DATA *)CmdHeaderPtr)->addressLo);
	bData = &((SPI16TARGET_DATA *)CmdHeaderPtr)->firstData;
	for (i = sizeof(SPI16TARGET_DATA) - 1; 
		i < ((SPITARGET_DATA *)CmdHeaderPtr)->len; i++)
		{
        temp_data = spi_transfer8_IF(cmdInterface,bData[j]);
        bData[j++] = temp_data;
		}

    //end transaction
    spi_end_transaction_IF(cmdInterface,SPI_DEASSERT_TIME);
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
spiTargetRead(): SPI read 16 bit address
-------------------------------------------------------------------*/
unsigned char spiTargetRead16()
{
    unsigned char i,j=0;
    unsigned char *bData;
    unsigned char temp_data;

    unsigned int cmdInterface;
    GET_SHARED_GLOBAL(cmdInterface,g_cmdInterface);
    //begin transaction
    spi_begin_transaction_IF(cmdInterface,DEVICE_INDEX,SPI_SPEED_KHZ,SPI_MODE);
    // Write address
    spi_transfer8_IF(cmdInterface,((SPI16TARGET_DATA *)CmdHeaderPtr)->addressHi);
    spi_transfer8_IF(cmdInterface,((SPI16TARGET_DATA *)CmdHeaderPtr)->addressLo);
	bData = &((SPI16TARGET_DATA *)CmdHeaderPtr)->firstData;
	for (i = sizeof(SPI16TARGET_DATA) - 1; 
		i < ((SPI16TARGET_DATA *)CmdHeaderPtr)->len; i++)
		{
        temp_data = spi_transfer8_IF(cmdInterface,bData[j]);
        bData[j++] = temp_data;
		}

    //end transaction
    spi_end_transaction_IF(cmdInterface,SPI_DEASSERT_TIME);
	return CMD_STATUS_PASS;
}

