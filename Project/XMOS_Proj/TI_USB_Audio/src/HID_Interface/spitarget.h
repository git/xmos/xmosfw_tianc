// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

//=============================================================================
// spitarget.h
//
// This module contains atrustures and macros used for implementing SPI 
// target cmds. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _SPITARGET_H
#define _SPITARGET_H

// Command codes
#define CMD_SPI_WRITE8	0x00
#define CMD_SPI_READ8	0x01
#define CMD_SPI_WRITE16	0x02
#define CMD_SPI_READ16	0x03

// Command structures
typedef struct _spitarget_data_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option sub address high byte of 16 bit address if applied
	unsigned char optionResult;

	// Sub-address
	unsigned char address;

	// first data
	unsigned char firstData;

} SPITARGET_DATA;

typedef struct _spita16rget_data_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option sub address high byte of 16 bit address if applied
	unsigned char optionResult;

	// Sub-address
	unsigned char addressHi;

	// Sub-address
	unsigned char addressLo;

	// first data
	unsigned char firstData;

} SPI16TARGET_DATA;


#endif // _SPITARGET_H
