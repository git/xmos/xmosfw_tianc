//================================================== 
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//================================================== 
/*================================================== 
gpiotarget.c: routines to handle gpio pins
//================================================*/
#include "cmds.h"
#include "gpiotarget.h"
#include <stddef.h>
#include <xs1.h>
#include <xccompat.h>
#include "xc_ptr.h"


static unsigned char gpioBank = 0;

extern void Bitset_Bank0_IF(unsigned mask);
extern void Bitclr_Bank0_IF(unsigned mask);
extern void write_Bank0_IF(unsigned value);
extern unsigned char Bitread_Bank0_IF();

// Function table
unsigned char (*GpioFcTable[])(void)= {
    gpioBitSet,
    gpioBitClear,
    gpioWrite,
    gpioRead,
    gpioSetBank,
    gpioGetBank,
    NULL
};

void devSetGpioBank(char bank)
{
    gpioBank = bank;
}

unsigned char devGetGpioBank(void)
{
    return gpioBank;
}

/*-------------------------------------------------------------------
gpioBitSet(): Bit set
-------------------------------------------------------------------*/
unsigned char gpioBitSet()
{
    unsigned char temp;

    temp = ((GPIO_DATA *)CmdHeaderPtr)->mask;

	switch(devGetGpioBank())
	{
		case 0:
		    Bitset_Bank0_IF(temp);
			break;

		case 1:
		    // Add Bank1 Interface
			break;

		case 2:
		    // Add Bank2 Interface
			break;

		default:
			break;
	}

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
gpioBitClear(): Bit clear
-------------------------------------------------------------------*/
unsigned char gpioBitClear()
{

    unsigned char temp;

    temp = ((GPIO_DATA *)CmdHeaderPtr)->mask;

	switch(devGetGpioBank())
	{
		case 0:
		    Bitclr_Bank0_IF(temp);
			break;

		case 1:

            // Add Bank1 Interface
			break;

		case 2:
		    // Add Bank2 Interface
			break;

		default:
			break;
	}

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
gpioWrite(): Write gpio pins
-------------------------------------------------------------------*/
unsigned char gpioWrite()
{
    unsigned char temp;

    temp = ((GPIO_DATA *)CmdHeaderPtr)->mask;


	switch(devGetGpioBank())
	{
		case 0:
		    write_Bank0_IF(temp);
			break;

		case 1:

            // Add Bank1 Interface
            break;

        case 2:
            // Add Bank2 Interface
            break;
		default:
			break;
	}

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
gpioRead(): Read gpio pins
-------------------------------------------------------------------*/
unsigned char gpioRead()
{

    unsigned char temp;

    temp = 0 ;


	switch(devGetGpioBank())
	{
		case 0:
		    temp = Bitread_Bank0_IF();
			break;
 	
		case 1:

            // Add Bank1 Interface
            break;

        case 2:
            // Add Bank2 Interface
            break;
	
		default:
			break;
	}
 	((GPIO_DATA *)CmdHeaderPtr)->mask = temp;

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
gpioSetBank(): Sets GPIO active bank
-------------------------------------------------------------------*/
unsigned char gpioSetBank()
{

   	unsigned char temp;

	temp = ((GPIO_DATA *)CmdHeaderPtr)->mask;

	devSetGpioBank(temp); 

	return CMD_STATUS_PASS;
}


/*-------------------------------------------------------------------
gpioGetBank(): Reads GPIO active bank
-------------------------------------------------------------------*/
unsigned char gpioGetBank()
{

	unsigned char temp;
	temp = devGetGpioBank();

	((GPIO_DATA *)CmdHeaderPtr)->mask = temp;

	return CMD_STATUS_PASS;
}





