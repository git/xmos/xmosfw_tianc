// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

//=============================================================================
// task1020regs.h
//
// This module contains atrustures and macros used for implementing Task1020 
// target cmds. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _TASK1020REGS_H
#define _TASK1020REGS_H

// 
// Command codes
// TAS1020 Register
#define CMD_TAS1020REGS_BITSET8			0x00
#define CMD_TAS1020REGS_BITCLEAR8		0x01
#define CMD_TAS1020REGS_WRITE8			0x02
#define CMD_TAS1020REGS_READ8			0x03
#define CMD_TAS1020REGS_GET_SAMPFREQ	0x04
#define CMD_TAS1020REGS_RESETDEVICE		0x05

#ifdef _TAS1020_WORD_ACCESS
#define CMD_TAS1020REGS_BITSET16		0xXX
#define CMD_TAS1020REGS_BITCLEAR16		0xXX
#define CMD_TAS1020REGS_WRITE16			0xXX
#define CMD_TAS1020REGS_READ16			0xXX
#define CMD_TAS1020REGS_BITSET32		0xXX
#define CMD_TAS1020REGS_BITCLEAR32		0xXX
#define CMD_TAS1020REGS_WRITE32			0xXX
#define CMD_TAS1020REGS_READ32			0xXX
#endif

// Command structures
typedef struct _task1020regs_bit8_struct {
	// Command length	
	unsigned char len;

	// command for  command set or clear bit
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// register Address low byte
	unsigned char regAddrHigh;

	// register Address high byte
	unsigned char regAddrLow;

	// 8 bit mask for bit set or bit clear
	unsigned char mask;
} TASK1020REGS_BIT8;

typedef struct _task1020regs_data8_struct {
	// Command length	
	unsigned char len;

	// command for  command set or clear bit
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// register Address low byte
	unsigned char regAddrLow;

	// register Address high byte
	unsigned char regAddrHigh;
	// First data
	unsigned char firstData;

} TASK1020REGS_DATA8;

typedef struct _task1020regs_freqdata_struct {
	// Command length	
	unsigned char len;

	// command for  command set or clear bit
	unsigned char cmdCode;

	// Command option, 
	unsigned char optionResult;

	// Sampling frequency
	unsigned char FreqByte2;
	unsigned char FreqByte1;
	unsigned char FreqByte0;

} TASK1020REGS_FREQDATA;

typedef struct _task1020regs_resetcpu_struct {
	// Command length	
	unsigned char len;

	// command for  command set or clear bit
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// register Address high byte
	unsigned char dummy;

} TASK1020REGS_RESETCPU;

#endif // _TASK1020REGS_H
