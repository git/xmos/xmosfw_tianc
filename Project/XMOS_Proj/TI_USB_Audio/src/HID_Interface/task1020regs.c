// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

// =============================================================================
// task1020regs.c
// -----------------------------------------------------------------------------
// Description:
//      Routines to handle read/writes commands to TAS1020 registers. Only
//      task1020GetSampFreq() is implemented for MSP430.
// =============================================================================

#include "cmds.h"
#include "task1020regs.h"
#include "xccompat.h"
#include "xc_ptr.h"

// Function prototypes
unsigned char task1020RegBitSet8();
unsigned char task1020RegBitClear8();
unsigned char task1020RegWrite8();
unsigned char task1020RegRead8();
unsigned char task1020GetSampFreq();
unsigned char task1020ResetCpu();


// Function table
unsigned char (*Task1020RegFcTable[])(void) = {
	task1020RegBitSet8,	task1020RegBitClear8,
	task1020RegWrite8, task1020RegRead8,
	// Get current sampling frequency
	task1020GetSampFreq,
	task1020ResetCpu,

#ifdef _TAS1020_WORD_ACCESS
/* Normally, tas1020 register access by byte,
	not used for now, HN 10/09/2008 */
// 16 bit registers
	task1020RegBitSet16, task1020RegBitClear16,
	task1020RegWrite16, task1020RegRead16,

// 32 bit registers
	task1020RegBitSet32,	task1020RegBitClear32,
	task1020RegWrite32, task1020RegRead32,
#endif
	
	0
}; 


/*-------------------------------------------------------------------
task1020RegBitSet8(): Set register bit byte size
-------------------------------------------------------------------*/
unsigned char task1020RegBitSet8()
{/*
	unsigned char volatile xdata *regPtr;

	regPtr = (((TASK1020REGS_BIT8 *)CmdHeaderPtr)->regAddrLow + 
              ((TASK1020REGS_BIT8 *)CmdHeaderPtr)->regAddrHigh << 8); 
	*regPtr = *regPtr | ((TASK1020REGS_BIT8 *)CmdHeaderPtr)->mask;*/
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
task1020RegBitClear8(): Clear register bit byte size
-------------------------------------------------------------------*/
unsigned char task1020RegBitClear8()
{/*
	unsigned char volatile xdata *regPtr;

	regPtr = (((TASK1020REGS_BIT8 *)CmdHeaderPtr)->regAddrLow + 
              ((TASK1020REGS_BIT8 *)CmdHeaderPtr)->regAddrHigh << 8); 
	*regPtr = *regPtr && ~((TASK1020REGS_BIT8 *)CmdHeaderPtr)->mask;*/
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
task1020RegWrite8(): Write register byte size
-------------------------------------------------------------------*/
unsigned char task1020RegWrite8()
{/*
	unsigned char volatile xdata *regPtr;
	unsigned char i;
	unsigned char*bPt;

	regPtr = (((TASK1020REGS_DATA8 *)CmdHeaderPtr)->regAddrLow + 
              ((TASK1020REGS_DATA8 *)CmdHeaderPtr)->regAddrHigh << 8); 
	bPt = &((TASK1020REGS_DATA8 *)CmdHeaderPtr)->firstData;
	i = ((TASK1020REGS_DATA8 *)CmdHeaderPtr)->len - sizeof(TASK1020REGS_DATA8) + 1;
	for (; i != 0 ;i--, regPtr++, bPt++)
	{
		*regPtr = *bPt;
	}
*/
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
task1020RegRead8(): Read register byte size
-------------------------------------------------------------------*/
unsigned char task1020RegRead8()
{/*
	unsigned char volatile xdata *regPtr;
	unsigned char i;
	unsigned char*bPt;

	regPtr = (((TASK1020REGS_DATA8 *)CmdHeaderPtr)->regAddrLow + 
              ((TASK1020REGS_DATA8 *)CmdHeaderPtr)->regAddrHigh << 8); 
	bPt = &((TASK1020REGS_DATA8 *)CmdHeaderPtr)->firstData;

	i = ((TASK1020REGS_DATA8 *)CmdHeaderPtr)->len - sizeof(TASK1020REGS_DATA8) + 1;
	for (; i != 0 ;i--, regPtr++, bPt++)
	{
		*bPt = *regPtr;
	}
*/
	return CMD_STATUS_PASS; 
}


/*-------------------------------------------------------------------
task1020GetSampFreq(): Get sampling frequency
-------------------------------------------------------------------*/
unsigned char task1020GetSampFreq()
{
    unsigned int sample_freq;
    GET_SHARED_GLOBAL(sample_freq,g_curSamFreq);
	((TASK1020REGS_FREQDATA *)CmdHeaderPtr)->FreqByte2 = sample_freq >> 16;
	((TASK1020REGS_FREQDATA *)CmdHeaderPtr)->FreqByte1 = (unsigned char)(sample_freq >> 8);
	((TASK1020REGS_FREQDATA *)CmdHeaderPtr)->FreqByte0 = (unsigned char)sample_freq;
 
	return CMD_STATUS_PASS; 
}


/*-------------------------------------------------------------------
task1020ResetCpu(): Set flag to reset CPU. The CPU will be reset in
main idle mode
-------------------------------------------------------------------*/
unsigned char task1020ResetCpu()
{/*
	HostRequestResetFlag = TRUE;*/
	return CMD_STATUS_PASS; 
}



