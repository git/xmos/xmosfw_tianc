//=============================================================================
// gpiotarget.h
//
// This module contains atrustures and macros used for implementing gpio 
// target cmds. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _GPIOTARGET_H
#define _GPIOTARGET_H

// Command codes
#define CMD_GPIO_BITSET		0x00
#define CMD_GPIO_BITCLEAR	0x01
#define CMD_GPIO_WRITE		0x02
#define CMD_GPIO_READ		0x03
#define CMD_GPIO_SETBANK	0x04
#define CMD_GPIO_GETBANK	0x05

// Command structures
typedef struct _gpio_data_struct {
	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// Bit map to be set to
	unsigned char mask;
} GPIO_DATA;

// Function prototypes
unsigned char gpioBitSet();
unsigned char gpioBitClear();
unsigned char gpioWrite();
unsigned char gpioRead();
unsigned char gpioSetBank();
unsigned char gpioGetBank();


#endif // _GPIOTARGET_H
