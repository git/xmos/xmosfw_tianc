// (c)2012 by Texas Instruments Incorporated, All Rights Reserved.

//=============================================================================
// aic3254.h
//
// This module contains atrustures and macros used for implementing aic3254 
// target cmds. 
//
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//============================================================================= 
#ifndef _AIC3254_H
#define _AIC3254_H

// Slave address
#define AIC3254_I2C_SLAVE_ADDRESS	0x30

// Command codes.
#define CMD_AI3254_SETPAGE	0x00
#define CMD_AI3254_GETPAGE	0x01 
#define CMD_AI3254_WRITE8	0x02
#define CMD_AI3254_READ8	0x03 

#ifdef _FOR_FUTURE
#define CMD_AI3254_WRITE16	0x04
#define CMD_AI3254_READ16	0x05 
#define CMD_AI3254_WRITE32	0x06
#define CMD_AI3254_READ32	0x07 
#endif

// Command structures
typedef struct _aic3254_page_data_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// Page to be set to
	unsigned char page;	// Page number to be set to
} AIC3254_PAGE_DATA;

// Command structures

typedef struct _aic3254_i2c_data_struct {

	// Command length	
	unsigned char len;

	// command code set page or get page
	unsigned char cmdCode;

	// Command option
	unsigned char optionResult;

	// Page to be set to
	unsigned char regAddr;	// Page number to be set to

	// first data
	unsigned char firstData;
} AIC3254_I2C_DATA;

#endif // _AIC3254_H
