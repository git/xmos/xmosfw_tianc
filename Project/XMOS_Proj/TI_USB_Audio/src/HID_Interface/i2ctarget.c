//================================================== 
// Texas Instruments Strictly Private 
// Copyright 2008, Texas Instruments Inc. 
//================================================== 
/*================================================== 
i2ctarget.c: routines to handle ead/write to I2C
//================================================*/
#include "cmds.h"
#include "i2ctarget.h"
#include <stddef.h>
#include "I2c.h"
#include "xc_ptr.h"

static unsigned char GlobSlaveAddress = 0;

// Function prototypes
unsigned char i2cTargetSetSlaveAddr();
unsigned char i2cTargetGetSlaveAddr();
unsigned char i2cTargetWrite();
unsigned char i2cTargetRead();
unsigned char i2cTargetWrite16();
unsigned char i2cTargetRead16();

typedef struct I2C_PARAMS
{
    unsigned char SlaveAddr;
    unsigned int SubAddr;
    unsigned char* pBuffer_I2C;
    unsigned int nLen;
    unsigned char Flags;

} I2C_PARAMS;


I2C_PARAMS AppI2CParms;

extern unsigned char I2C_Access_IF(CLIENT_INTERFACE(i_cmd, cmdInterface), unsigned char* databuf,
        unsigned char slaveAddr,unsigned int SubAddr, unsigned int nLen,unsigned char Flags);
extern void I2C_Init_IF(CLIENT_INTERFACE(i_cmd, cmdInterface),unsigned int bit_time);


// Function table
unsigned char (*I2cTargetFcTable[])(void) = {
	i2cTargetSetSlaveAddr,
	i2cTargetGetSlaveAddr,
	i2cTargetWrite,
	i2cTargetRead, 
	i2cTargetWrite16,
	i2cTargetRead16, 
	NULL
}; 
// External reference
unsigned char I2CAccess()
{

    unsigned int cmdInterface;
    GET_SHARED_GLOBAL(cmdInterface,g_cmdInterface);

    return( I2C_Access_IF(cmdInterface,
                    AppI2CParms.pBuffer_I2C,
                    AppI2CParms.SlaveAddr,
                    AppI2CParms.SubAddr,
                    AppI2CParms.nLen,
                    AppI2CParms.Flags));

}



/*-------------------------------------------------------------------
i2cTargetSetSlaveAddr(): I2C Set slave address
-------------------------------------------------------------------*/
unsigned char i2cTargetSetSlaveAddr()
{
    unsigned int I2C_bit_time = 1000;
    unsigned int cmdInterface;
    GET_SHARED_GLOBAL(cmdInterface,g_cmdInterface);

	GlobSlaveAddress = ((I2CTARGET_SLAVEADDR *)CmdHeaderPtr)->slaveAddess;
	GlobSlaveAddress = GlobSlaveAddress >> 1;
	I2C_bit_time =	((I2CTARGET_SLAVEADDR *)CmdHeaderPtr)->optionResult & I2CTARGET_OPTION_100KHZ? 1000 : 250;
	I2C_Init_IF(cmdInterface,I2C_bit_time);
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
i2cTargetGetSlaveAddr(): I2C Get slave address
-------------------------------------------------------------------*/
unsigned char i2cTargetGetSlaveAddr()
{
    // Slave address is left shifted by 1 assuming read/write bit as 0
    // This is done to maintain the compatibility with TAS1020B
	((I2CTARGET_SLAVEADDR *)CmdHeaderPtr)->slaveAddess = GlobSlaveAddress << 1;
	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
i2cTargetWrite(): I2C write
-------------------------------------------------------------------*/
unsigned char i2cTargetWrite()
{
	APSET_I2C(
		GlobSlaveAddress,
		((I2CTARGET_DATA8 *)CmdHeaderPtr)->subAddess,
		&((I2CTARGET_DATA8 *)CmdHeaderPtr)->firstData,
	 	((I2CTARGET_DATA8 *)CmdHeaderPtr)->len - sizeof(I2CTARGET_DATA8) + 1,
		 I2C_WRITE );

	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
i2cTargetRead(): I2C read
-------------------------------------------------------------------*/
unsigned char i2cTargetRead()
{
	APSET_I2C(
		GlobSlaveAddress,
		((I2CTARGET_DATA8 *)CmdHeaderPtr)->subAddess,
		&((I2CTARGET_DATA8 *)CmdHeaderPtr)->firstData,
	 	((I2CTARGET_DATA8 *)CmdHeaderPtr)->len - sizeof(I2CTARGET_DATA8) + 1,
		 I2C_READ);

	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
i2cTargetWrite16(): I2C write 16 bit address
-------------------------------------------------------------------*/
unsigned char i2cTargetWrite16()
{
	unsigned int subAddress;

	subAddress = (((unsigned int )((I2CTARGET_DATA16 *)CmdHeaderPtr)->subAddessHi << 8) & 0xFF00) |
				 ((unsigned int )((I2CTARGET_DATA16 *)CmdHeaderPtr)->subAddessLo & 0x00FF);
	APSET_I2C(
		GlobSlaveAddress,
		subAddress,
		&((I2CTARGET_DATA16 *)CmdHeaderPtr)->firstData,
	 	((I2CTARGET_DATA16 *)CmdHeaderPtr)->len - sizeof(I2CTARGET_DATA16) + 1,
		I2C_WRITE | I2C_WORD_ADDR_TYPE);

	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}

/*-------------------------------------------------------------------
i2cTargetRead16(): I2C read 16 bit address
-------------------------------------------------------------------*/
unsigned char i2cTargetRead16()
{
    unsigned int subAddress;

	subAddress = (((unsigned int )((I2CTARGET_DATA16 *)CmdHeaderPtr)->subAddessHi << 8) & 0xFF00) |
				 ((unsigned int )((I2CTARGET_DATA16 *)CmdHeaderPtr)->subAddessLo & 0x00FF);
	APSET_I2C(
		GlobSlaveAddress,
		subAddress,
		&((I2CTARGET_DATA16 *)CmdHeaderPtr)->firstData,
	 	((I2CTARGET_DATA16 *)CmdHeaderPtr)->len - sizeof(I2CTARGET_DATA16) + 1,
		I2C_READ | I2C_WORD_ADDR_TYPE);

	if (!I2CAccess())
		return CMD_STATUS_FAIL;

	return CMD_STATUS_PASS;
}
