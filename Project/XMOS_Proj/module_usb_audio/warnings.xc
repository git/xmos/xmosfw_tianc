
/*
Warnings relating to configuration defines located in this XC source file rather than the devicedefines.h header file in order to avoid multiple warnings being issued when the devicedefines.h header file is included in multiple files.
*/

#include "customdefines.h"

#include "customdefines.h"

#ifndef DEFAULT_FREQ
#warning DEFAULT_FREQ not defined. Using MIN_FREQ
#endif

#ifndef MIN_FREQ
#warning MIN_FREQ not defined. Using 44100
#endif

#ifndef MAX_FREQ
#warning MAX_FREQ not defined. Using 192000
#endif

#ifndef VENDOR_STR
#warning VENDOR_STR not defined. Using "Texas Instruments"
#endif

#ifndef BCD_DEVICE
#warning BCD_DEVICE not defined
#endif



