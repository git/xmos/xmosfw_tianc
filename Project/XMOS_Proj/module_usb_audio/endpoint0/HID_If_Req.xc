#include <xs1.h>
#include <safestring.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "xud.h"                 /* XUD user defines and functions */
#include "hid.h"
#include "usb_std_requests.h"
#include "cmds.h"

#include"cmd_interface.h"
#include"HID_If_Req.h"
#include "cmdparse.h"

unsigned datalength_hid;
unsigned char buffer_hid[512];

int HidInterfaceClassRequests(XUD_ep ep0_out, XUD_ep &?ep0_in, XUD_ep &?ep_hid_in, USB_SetupPacket_t &sp,chanend c_hid)
{

    XUD_Result_t result=XUD_RES_ERR;

    switch(sp.bRequest)
    {
        case HID_GET_REPORT:

            /* Mandatory. Allows sending of report over control pipe */
            /* Send a hid report - note the use of unsafe due to shared mem */
//            unsafe {
//              char * unsafe p_reportBuffer = buffer;
//              buffer[0] = p_reportBuffer[0];
//            }

            result =  XUD_DoGetRequest(ep0_out, ep0_in, (buffer_hid, unsigned char []), 64, sp.wLength);
            break;

        case HID_GET_IDLE:
            /* Return the current Idle rate - optional for a HID mouse */

            /* Do nothing - i.e. STALL */
            break;

        case HID_GET_PROTOCOL:
            /* Required only devices supporting boot protocol devices,
             * which this example does not */

            /* Do nothing - i.e. STALL */
            break;

         case HID_SET_REPORT:

             if(sp.bmRequestType.Direction == USB_BM_REQTYPE_DIRECTION_H2D)
             {
                // memset(buffer_hid,0x55,sizeof(buffer_hid));
                 // Host to device
                 if (sp.wLength)
                     result =  XUD_GetBuffer(ep0_out, (buffer_hid, unsigned char[]), datalength_hid);

             }

             //Acknowlwdge set request
             if(result == XUD_RES_OKAY)
                 result = XUD_DoSetRequestStatus(ep0_in);

              if(result == XUD_RES_OKAY)
              {
                  //HID Interface command Parser
                  cmdsParser();

                  // Send the results back through HID
                  result = XUD_SetReady_In(ep_hid_in,buffer_hid,64);

                  // Wait till data is sent
                  while(1)
                  {
                      select
                      {
                      case XUD_SetData_Select ( c_hid , ep_hid_in , result ):
                                return result;
                      break ;
                      }
                  }


              }

            break;


        case HID_SET_IDLE:
            /* Set the current Idle rate - this is optional for a HID mouse
             * (Bandwidth can be saved by limiting the frequency that an
             * interrupt IN EP when the data hasn't changed since the last
             * report */

            /* Do nothing - i.e. STALL */
            break;

        case HID_SET_PROTOCOL:
            /* Required only devices supporting boot protocol devices,
             * which this example does not */

            /* Do nothing - i.e. STALL */
            break;
        default:
            result = XUD_RES_ERR;
            break;
    }

    return result;
}
