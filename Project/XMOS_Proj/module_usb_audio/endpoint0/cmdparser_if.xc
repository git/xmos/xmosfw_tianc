/*
 * cmdparser_if.xc
 *
 *  Created on: 08-Jul-2016
 *      Author: x0211286
 */

#include "cmd_interface.h"
#include "i2c.h"
#include "i2c_shared.h"
#include "xc_ptr.h"
#include "xs1.h"
#include"spi.h"

extern tileref tile[];
on tile [0] : struct r_i2c r_i2c = {XS1_PORT_4A};

unsigned char buffer_test[512];
unsigned char test_global_p=0;
unsigned char test_global=0;
void cmdparserHandler(server interface i_cmd cmd)
{

    unsigned char data[64];

    //Default Initialization with 100KHz
    i2c_shared_master_init(r_i2c,1000);
    // Initialize SPI here

    spi_master_init();

    while(1)
    {
        select
        {
            case cmd.test():
              unsafe
              {
                  buffer_test[test_global_p--] = test_global--;
              }
              break;
            case cmd.cmd_I2C_Init(unsigned int bit_time):
                i2c_shared_master_init(r_i2c,bit_time);
                break;
            case cmd.cmd_I2CAccess(unsigned char databuff[],unsigned char slaveAddr,unsigned int SubAddr,unsigned int nLen,unsigned char Flags) -> unsigned char result:

                if (Flags & I2C_WRITE)
                {
                    for(int i=0;i<nLen;i++)
                    {
                        data[i]=databuff[i];
                    }
                    //
                    // Write phase; write out slave address, subaddress and data
                    //
                    if(Flags & I2C_WORD_ADDR_TYPE)
                        result = i2c_shared_master_write_reg_16(r_i2c, slaveAddr,SubAddr, data, nLen);
                    else
                        result = i2c_shared_master_write_reg(r_i2c, slaveAddr,SubAddr, data, nLen);


                }

                if (Flags & I2C_READ)
                {
                    if(Flags & I2C_WORD_ADDR_TYPE)
                        result = i2c_shared_master_read_reg_16(r_i2c, slaveAddr,SubAddr, data, nLen);
                    else
                        result = i2c_shared_master_read_reg(r_i2c, slaveAddr,SubAddr, data, nLen);

                    for(int i=0;i<nLen;i++)
                    {
                        databuff[i]=data[i];
                    }
                }

                break;
            case cmd.cmd_spi_begin_transaction(unsigned device_index,unsigned speed_in_khz, spi_mode_t mode):
                    spi_begin_transaction(device_index,speed_in_khz,mode);
                    break;

            case cmd.cmd_spi_end_transaction(unsigned ss_deassert_time):
                    spi_end_transaction(ss_deassert_time);
                break;
            case cmd.cmd_spi_transfer8(unsigned char data)-> unsigned char result:
                    result = spi_transfer8(data);
                    break;
            case cmd.done():

                    break;
        }

    }

}
