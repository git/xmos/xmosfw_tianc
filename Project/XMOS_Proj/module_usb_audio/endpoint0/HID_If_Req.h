
#ifndef _ENDPOINT0_H_
#define _ENDPOINT0_H_

#include "xud.h"
#include <xccompat.h>

int HidInterfaceClassRequests(XUD_ep ep0_out, NULLABLE_REFERENCE_PARAM(XUD_ep, ep0_in),NULLABLE_REFERENCE_PARAM(XUD_ep, ep_hid_in),
                                REFERENCE_PARAM(USB_SetupPacket_t, sp),chanend c_hid);

#endif
