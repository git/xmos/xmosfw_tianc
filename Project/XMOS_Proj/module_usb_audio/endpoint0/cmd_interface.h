/*
 * cmd_interface.h
 *
 *  Created on: 08-Jul-2016
 *      Author: x0211286
 */

#include "i2ctarget.h"
#include "spi.h"

#ifndef CMD_INTERFACE_H_
#define CMD_INTERFACE_H_

interface i_cmd
{
    void done();
    unsigned char cmd_I2CAccess(unsigned char databuff[],unsigned char slaveAddr,unsigned int SubAddr,unsigned int nLen,unsigned char Flags);
    void test();
    void cmd_I2C_Init(unsigned int bit_time);
    void cmd_spi_begin_transaction(unsigned device_index,unsigned speed_in_khz, spi_mode_t mode);
    void cmd_spi_end_transaction(unsigned ss_deassert_time);
    unsigned char cmd_spi_transfer8(unsigned char data);
};

void cmdparserHandler(server interface i_cmd cmd);

#endif /* CMD_INTERFACE_H_ */
