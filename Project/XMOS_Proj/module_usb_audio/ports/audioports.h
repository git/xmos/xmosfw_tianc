#ifndef _AUDIOPORTS_H_
#define _AUDIOPORTS_H_

#include <xccompat.h>
#include "devicedefines.h"

#ifdef __XC__
void ConfigAudioPorts(
                buffered out port:32 p_i2s_dac[],
                int numDacPorts,

                buffered in port:32  p_i2s_adc[],
                int numAdcPorts,
                buffered out port:32 ?p_lrclk,
                buffered out port:32 p_bclk,
                unsigned int divide, unsigned int curSamFreq);
#else

void ConfigAudioPorts(
                port p_i2s_dac[],
                int numDacPorts,
                port p_i2s_adc[],
                int numAdcPorts,
                port p_lrclk,
                port p_bclk,
                unsigned int divide, unsigned int curSamFreq);


#endif /* __XC__*/


#ifdef __XC__
void ConfigAudioPortsWrapper(
                buffered out port:32 p_i2s_dac[], int numPortsDAC,
                buffered in port:32  p_i2s_adc[], int numPortsADC,
                buffered out port:32 ?p_lrclk,
                buffered out port:32 p_bclk,
                unsigned int divide, unsigned curSamFreq, unsigned int dsdMode);
#else

void ConfigAudioPortsWrapper(
                port p_i2s_dac[], int numPortsDAC,
                port p_i2s_adc[], int numPortsADC,
                port p_lrclk,
                port p_bclk,
                unsigned int divide, unsigned curSamFreq, unsigned int dsdMode);


#endif /* __XC__*/

#ifdef __XC__
void EnableBufferedPort(buffered out port:32 p, unsigned transferWidth);
#else
void EnableBufferedPort(port p, unsigned transferWidth);
#endif

#endif /* _AUDIOPORTS_H_ */
